// swift-tools-version: 5.9
import PackageDescription


let package = Package(
	name: "executable-and-target-name-differ",
	products: [.executable(name: "my-executable", targets: ["MyTarget"])],
	targets: [
		.executableTarget(name: "MyTarget"),
		.testTarget(name: "MyTargetTests", dependencies: [.target(name: "MyTarget")])
	]
)
